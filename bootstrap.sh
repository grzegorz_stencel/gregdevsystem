# bootstrap.sh precisely the same things that we would like to have puppet or chef do.
# Created by Grzegorz Stencel

# Changes to this require a 'vagrant provision'
USER=vagrant
SYNC_FOLDER='/vagrant'

echo "bootstrap start"

set -x
set -e # exit on error


echo "---------------------------------------------"
echo "UPDATING atp repo"
echo "---------------------------------------------"
sudo apt-get update 
echo "---------------------------------------------"
echo "INSTALLING sys packages"
echo "---------------------------------------------"
sudo apt-get install -y language-pack-en
sudo locale-gen en_GB.UTF-8
sudo apt-get install -y $(cat $SYNC_FOLDER/system-requirements.txt)
sudo debconf-set-selections <<< 'mysql-server-5.7 mysql-server/root_password password django' && sudo debconf-set-selections <<< 'mysql-server-5.7 mysql-server/root_password_again password django' && sudo apt-get -y install mysql-server-5.7
echo "---------------------------------------------"
echo "INSTALLING python pckgs"
echo "---------------------------------------------"
sudo pip install -r $SYNC_FOLDER/python-requirements.txt
#sudo sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O - --no-check-certificate)"
#curl https://raw.githubusercontent.com/onjin/vim-startup/master/startup.sh | bash
#cp $SYNC_FOLDER/conf/.zshrc /home/vagrant/.zshrc
#cp $SYNC_FOLDER/conf/.vimrc /home/vagrant/.vimrc
#cp $SYNC_FOLDER/conf/rc.lua /home/vagrant/.config/awesome/rc.lua
#git clone git://github.com/Shougo/neobundle.vim /home/vagrant/.vim/bundle/neobundle.vim
#wget https://raw.githubusercontent.com/greg4fun/vim-startup/master/.vimrc -O /home/vagrant/.vimrc
mkdir -p /home/vagrant/.fonts
wget https://github.com/greg4fun/vim-startup/raw/master/fonts/Anonymous%20Pro.ttf -O /home/vagrant/.fonts/Anonymous\ Pro.ttf
wget https://github.com/greg4fun/vim-startup/raw/master/fonts/Inconsolata.otf -O /home/vagrant/.fonts/Inconsolata.otf

#latest:
#sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
sh -c "$(wget https://raw.githubusercontent.com/greg4fun/vim-python-ide/master/install.sh -O - --no-check-certificate)"
